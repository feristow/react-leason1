import React from 'react';
import './UserOutput.css';

const UserOutput = (props) => {
    const { username } = props;
    return (
        <div className="UserOutput">
            <p>{username.toUpperCase()}</p>
        </div>
    );
};

export default UserOutput;