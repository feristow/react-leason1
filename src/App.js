import React, { Component } from 'react';
import './App.css';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

class App extends Component {
  
  state = {
    username: 'feristow'
  };

  usernameChangeHandler = (event) => {
    this.setState({ username: event.target.value });
  };

  render() {
    const { username } = this.state;
    return (
      <div className="App">
        <h1>The React App - Leason 1</h1>
        <UserInput username={username} changed={this.usernameChangeHandler}/>
        <UserOutput username={username} />
      </div>
    );
  }

}

export default App;
