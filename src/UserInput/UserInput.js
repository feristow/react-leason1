import React, { Component } from 'react';
import './UserInput.css';

class UserInput extends Component {
    
    render() {
        const { username, changed } = this.props;
        return (
            <div className="UserInput">
                <label>Digite o usuário</label>
                <input type="text" value={username} onChange={changed} autofocus="true"/>
            </div>
        );
    }
}

export default UserInput;